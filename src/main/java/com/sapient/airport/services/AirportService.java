package com.sapient.airport.services;

import com.sapient.airport.db.IAirportsDatabase;

/**
 * dependecy mocking
 * How in the world i would be able to test AirportService without AirportsDatabase
 */
public class AirportService {
    /**
     * Dependencies are the bigger chunk of issue when testing
     * So You must have a way to mock the dependencies
     */
    private IAirportsDatabase database = null;

    public AirportService(IAirportsDatabase database) {
        this.database = database;
    }
    public int countAirports() {
        // these are the line of code that i wish to test .. and not
        // what is there in database.getAirports()
        return this.database.getAirports().size();
    }
}
