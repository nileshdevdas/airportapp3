package com.sapient.airport.jmx;


import com.sapient.airport.db.AirportsDatabase;

import java.util.Random;

public class DBOperations implements IDBOperations {
    private int recordCount;

    public void reloadDatabase() {
        AirportsDatabase.getInstance().reloadDB();
    }

    @Override
    public void empty() {
        AirportsDatabase.getInstance().empty();
    }

    public int getRecordCount() {
        return AirportsDatabase.getInstance().getAirports().size();
    }
}
