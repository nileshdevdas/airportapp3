package com.sapient.airport.launcher;

import com.sapient.airport.jmx.DBOperations;
import com.sapient.airport.jmx.IDBOperations;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.management.MBeanServer;
import javax.management.ObjectName;
import javax.management.StandardMBean;
import java.lang.management.ManagementFactory;

public class Main {

    private static final Logger logger = LoggerFactory.getLogger(Main.class);
    public static void main(String[] args) throws  Exception{
       logger.info("Start");
        System.setProperty("db_file", "d:\\workspace\\airportsapp\\src\\main\\resources\\airports.csv");
        ObjectName myMbean = new ObjectName("com.cog.javalearning:type=basic,name=DBOperations");
        MBeanServer server = ManagementFactory.getPlatformMBeanServer();
        StandardMBean mbean = new StandardMBean(new DBOperations(), IDBOperations.class);
        server.registerMBean(mbean, myMbean);
        logger.info("Started.");
        Thread.sleep(Long.MAX_VALUE);
    }
}
