package com.sapient.airport.utils;

import com.sapient.airport.model.Airport;

public class AirportTranslator {

    private AirportTranslator(){}

    public static Airport stringToAirport(String airportLine){
        String stripedLine = airportLine.strip();
        String plainText = stripedLine.replaceAll("\"", "");
        String[] splitData = plainText.split(",");
        Airport airport  = new Airport();
        airport.setName(splitData[3]);
        return airport;
    }
}
